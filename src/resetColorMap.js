
import { getEnabledElement } from './enabledElements.js';

/**
 * Resets the color map and reinitialize the canvas to fix the
 * issue of https://github.com/cornerstonejs/cornerstone/issues/317
 *
 * @param {HTMLElement} element An HTML Element enabled for Cornerstone
 * @returns {void}
 */
export default function (element) {
  const enabledElement = getEnabledElement(element);

  enabledElement.renderingTools.colormapId = undefined;
  enabledElement.renderingTools.colorLut = undefined;

  const renderCanvas = enabledElement.renderingTools.renderCanvas;
  const canvasContext = renderCanvas.getContext('2d');

  // NOTE - we need to fill the render canvas with white pixels since we
  // control the luminance using the alpha channel to improve rendering performance.
  canvasContext.fillStyle = 'white';
  canvasContext.fillRect(
    0,
    0,
    renderCanvas.width,
    renderCanvas.height
  );

  const renderCanvasData = canvasContext.getImageData(
    0,
    0,
    renderCanvas.width,
    renderCanvas.height
  );

  enabledElement.renderingTools.renderCanvasContext = canvasContext;
  enabledElement.renderingTools.renderCanvasData = renderCanvasData;
}
