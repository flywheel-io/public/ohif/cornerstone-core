import { getColormap, getColormapsList } from './colormap.js';
import {getColorChannelList,getColorChannel} from './colorChannel.js';
import LookupTable from './lookupTable.js';

export default {
  getColormap,
  getColormapsList,
  LookupTable,
  getColorChannelList,
  getColorChannel,
};
