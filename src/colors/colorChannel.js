const colorChannelData = [
  {
    id: "rgb",
    name: "RGB",
  },
  {
    id: "red",
    name: "Red",
  },
  {
    id: "green",
    name: "Green",
  },
  {
    id: "blue",
    name: "Blue",
  },
];

export function getColorChannelList() {
  return colorChannelData;
}

export function getColorChannel(id, channelData) {
  let colorChannel = colorChannelData.find((channel) => channel.id === id);

  return {
    getId() {
      return id;
    },

    getColorChannelSchemeName() {
      return colorChannel.name;
    },
  };
}
