const webpack = require('webpack');
const merge = require('./merge');
const baseConfig = require('./webpack-base');
const TerserPlugin = require("terser-webpack-plugin");

const devConfig = {
  output: {
    filename: '[name].min.js'
  },
  mode: "production",
  optimization: {
    minimizer: [
      new TerserPlugin(),
    ]
  },
};

module.exports = merge(baseConfig, devConfig);
